// Use the "require" directive to load the express module/package
const express = require('express');

// Create an application express
const app = express();

// For our application server to run, we need a port to listen to
const port = 3000;

// Allows your app to read json data
app.use(express.json());

// Allows your app to read other data types
// {extended: true} - by applying option, it allows us to receive information in other data types
app.use(express.urlencoded({extended: true}));


// Routes
// Express has methods corresponding to each HTTP method

// GET
// This route expects to receive a GET request at URI "/greet"
app.get('/greet', (req, res) => {
	// res.send - sends a response back to the client
	res.send("Hello from the /greet endpoint!");
});

// POST
app.post('/hello', (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`);
});

// Simple Registration Form

let users = [];

app.post('/signup', (req, res) => {

	if(req.body.username !== '' && req.body.password !== '') {
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered!`);
		console.log(users);
	} else {
		res.send("Please input both username and password.");
	}
})

// Change password
app.patch("/change-password", (req, res) => {
	// Creates a variable to store the message to be sent back to the client
	// let message;
	// for(let i = 0; i < users.length; i++) {
	// 	if(req.body.username == users[i].username) {
	// 		users[i].password = req.body.password;

	// 		message = `User ${req.body.username}'s password has been updated.`;
	// 		break;
	// 	} else {
	// 		message = `User does not exist.`;
	// 	}
	// }
	
	// If th username provided in the Postman and the username of the current object in the loop is the same
	for (let user of users) {
		if(req.body.username == user.username) {
			// Change password
			user.password = req.body.password;

			// message to be sent back if password has been updated successfully
			message = `User ${req.body.username}'s password has been updated.`;
			break;
			// else no user match
		} else {
			message = `User does not exist.`;
		}
	}
	res.send(message);
})











// Tells our server to listen to a port
app.listen(port, () => console.log(`Server runnnig at port ${port}`));

